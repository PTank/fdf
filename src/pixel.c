/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pixel.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:46:53 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:16:33 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

t_pixel	*init_pixel(int y, int x, int z)
{
	t_pixel	*new_pixel;

	new_pixel = ft_memalloc(sizeof(t_pixel));
	if (!new_pixel)
		return (0);
	new_pixel->z = z;
	new_pixel->x = x;
	new_pixel->y = y;
	new_pixel->up = NULL;
	new_pixel->down = NULL;
	new_pixel->right = NULL;
	new_pixel->up = NULL;
	return (new_pixel);
}

t_pixel	*add_node(t_pixel *old, t_pixel *current, t_map *map, int x)
{
	t_pixel	*y;
	int		x2;

	x2 = 0;
	y = map->head;
	if (old)
	{
		old->right = current;
		current->left = old;
	}
	while (y->down && y->y < map->h - 1)
		y = y->down;
	while (x2 < x && y)
	{
		y = y->right;
		x2++;
	}
	if (y && y->y != current->y)
	{
		y->down = current;
		current->up = y;
	}
	return (current);
}
