/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:47:11 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:23:10 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int	put_color(char *addr, int color)
{
	addr[0] = (color & 0xFF);
	addr[1] = ((color & 0xFF00) >> 8);
	addr[2] = ((color & 0xFF0000) >> 16);
	return (0);
}

int	color_all_img(char *addr, int color, int sizeline, int y)
{
	int	i;

	i = 0;
	while (i <= sizeline * y)
	{
		put_color(addr + i, color);
		i += 4;
	}
	return (0);
}

int	put_color_xy(t_img *img, int color, int x, int y)
{
	int	pos;
	int	d;

	d = SIZE_Y_FDF;
	if (y < -100 && y > (img->sizeline * SIZE_Y_FDF + 100))
		return (-2);
	pos = y * img->sizeline + (32 / 8) * x;
	if (pos > ((y + 1) * img->sizeline) || pos < (y * img->sizeline))
		return (-1);
	if (pos >= 0 && pos <= (img->sizeline * d))
		put_color(img->addr + pos, color);
	return (0);
}
