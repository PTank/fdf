/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   send_pix.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:47:21 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:14:57 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static t_pixel	*addxy(t_pixel *pix, int x, int y)
{
	t_pixel	*n_pix;

	n_pix = (t_pixel *)ft_memalloc(sizeof(t_pixel));
	n_pix->x = x;
	n_pix->y = y;
	n_pix->z = pix->z;
	return (n_pix);
}

int				send_pix_right(t_pixel *pixel, int cy, t_mlx *mlx)
{
	int		cx;
	t_pixel	*p1;
	t_pixel	*p2;
	t_pixel	*pix;

	pix = pixel;
	cx = CENTER_X;
	while (pix && cx <= SIZE_X_FDF * 3)
	{
		p1 = addxy(pix, cx, cy);
		if (pix->down)
		{
			p2 = addxy(pix->down, cx, cy + mlx->pos->dist);
			make_iso(p1, p2, mlx);
		}
		if (pix->right)
		{
			p2 = addxy(pix->right, cx + mlx->pos->dist, cy);
			make_iso(p1, p2, mlx);
		}
		ft_memdel((void **)&p1);
		pix = pix->right;
		cx += mlx->pos->dist;
	}
	return (0);
}

int				send_pix_left(t_pixel *pixel, int cy, t_mlx *mlx)
{
	int		cx;
	t_pixel	*p1;
	t_pixel	*p2;
	t_pixel	*pix;

	pix = pixel;
	cx = CENTER_X;
	while (pix && cx >= -(SIZE_X_FDF))
	{
		p1 = addxy(pix, cx, cy);
		if (pix->down)
		{
			p2 = addxy(pix->down, cx, cy + mlx->pos->dist);
			make_iso(p1, p2, mlx);
		}
		if (pix->left)
		{
			p2 = addxy(pix->left, cx - mlx->pos->dist, cy);
			make_iso(p1, p2, mlx);
		}
		pix = pix->left;
		cx -= mlx->pos->dist;
	}
	return (0);
}
