/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:46:22 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:10:01 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static t_map	*init_map(void)
{
	t_map	*map;

	map = (t_map *)ft_memalloc(sizeof(t_map));
	if (!map)
		return (NULL);
	map->head = NULL;
	map->h = 0;
	map->w = 0;
	return (map);
}

static int		del_split(char **split, char **line)
{
	int	i;

	i = 0;
	while (split[i])
	{
		ft_memdel((void **)&split[i]);
		i++;
	}
	ft_memdel((void **)&split);
	ft_memdel((void **)line);
	return (0);
}

static int		read_file(int fd, t_map *map, t_pixel *current)
{
	char	*line;
	char	**split;
	t_pixel	*old;
	int		x;

	while (get_next_line(fd, &line) > 0)
	{
		old = NULL;
		split = ft_strsplit(line, ' ');
		x = 0;
		while (split[x])
		{
			current = init_pixel(map->h, x, ft_atoi(split[x]));
			if (!map->h && !x)
				map->head = current;
			else
				current = add_node(old, current, map, x);
			old = current;
			x++;
		}
		del_split(split, &line);
		map->h++;
		map->w = (map->w < x) ? x : map->w;
	}
	return (0);
}

t_map			*parse_file_to_map(char *filename)
{
	int		fd;
	t_map	*map;
	t_pixel	*current;

	current = NULL;
	fd = open(filename, O_RDONLY);
	map = init_map();
	if (fd < 0 || !map)
		return (NULL);
	map->h = 0;
	read_file(fd, map, current);
	if (!map->h)
		return (NULL);
	close(fd);
	return (map);
}

int				destroy_map(t_map *map)
{
	t_pixel	*y;
	t_pixel	*x;
	t_pixel	*x2;

	y = map->head;
	while (y)
	{
		x = y;
		while (x)
		{
			x2 = x;
			x = x->right;
			ft_memdel((void **)&x2);
		}
		y = y->down;
	}
	ft_memdel((void **)&map);
	return (0);
}
