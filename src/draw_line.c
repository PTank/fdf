/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:45:15 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:17:39 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static int	put_linex(t_iso *iso, t_img *img, int color)
{
	int	x;
	int	y;
	int	div;

	x = iso->x1;
	y = 0;
	div = (iso->x2 - iso->x1 != 0) ? (iso->x2 - iso->x1) : 1;
	while (x <= iso->x2)
	{
		y = iso->y1 + ((iso->y2 - iso->y1) * (x - iso->x1))
			/ div;
		if ((put_color_xy(img, color, x, y)))
			return (-1);
		x++;
	}
	return (0);
}

static int	put_liney(t_iso *iso, t_img *img, int color)
{
	int	y;
	int	x;
	int	div;

	y = iso->y1;
	x = 0;
	div = (iso->y2 - iso->y1 != 0) ? (iso->y2 - iso->y1) : 1;
	while (y <= iso->y2)
	{
		x = iso->x1 + ((y - iso->y1) * (iso->x2 - iso->x1))
			/ div;
		if ((put_color_xy(img, color, x, y)))
			return (-1);
		y++;
	}
	return (0);
}

static void	inverse_iso(t_iso *iso, t_iso *new)
{
	new->x1 = iso->x2;
	new->y1 = iso->y2;
	new->x2 = iso->x1;
	new->y2 = iso->y1;
}

int			draw_line(t_iso *iso, t_mlx *mlx, t_img *img)
{
	int		y;
	int		color;
	t_iso	inv_iso;

	if (iso->z1 > 0 && iso->z2 > 0)
		color = mlx_get_color_value(mlx->mlx, GREY);
	else if (iso->z1 < 0 && iso->z2 < 0)
		color = mlx_get_color_value(mlx->mlx, BLUE);
	else
		color = mlx_get_color_value(mlx->mlx, RED);
	inverse_iso(iso, &inv_iso);
	y = abs(iso->y2 - iso->y1);
	if (iso->x1 <= iso->x2 && (iso->x2 - iso->x1) >= y)
		return (put_linex(iso, img, color));
	else if (abs(iso->x2 - iso->x1) >= y)
		return (put_linex(&inv_iso, img, color));
	else if (iso->y1 <= iso->y2)
		return (put_liney(iso, img, color));
	else
		return (put_liney(&inv_iso, img, color));
	return (0);
}
