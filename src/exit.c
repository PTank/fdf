/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:45:26 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 17:45:28 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int	exit_fdf(t_mlx *mlx)
{
	if (!mlx)
		exit(-1);
	mlx_destroy_window(mlx->mlx, mlx->mlx_win);
	destroy_map(mlx->map);
	destroy_img(mlx->menu);
	destroy_img(mlx->fdf);
	ft_memdel((void **)&mlx->pos);
	ft_memdel((void **)&mlx);
	exit(0);
	return (0);
}
