/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_fdf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:47:04 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:24:47 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int				make_iso(t_pixel *pix1, t_pixel *pix2, t_mlx *mlx)
{
	t_iso	*iso;
	t_iso	iso1;
	t_iso	iso2;
	int		ret;

	ret = 0;
	iso = ft_memalloc(sizeof(t_iso));
	iso_simple(pix1, &iso1, mlx);
	iso_simple(pix2, &iso2, mlx);
	iso->x1 = iso2.x1;
	iso->y1 = iso2.y1;
	iso->z1 = iso2.z1;
	iso->x2 = iso1.x1;
	iso->y2 = iso1.y1;
	iso->z2 = iso1.z1;
	ret = draw_line(iso, mlx, mlx->fdf);
	ft_memdel((void **)&pix2);
	ft_memdel((void **)&iso);
	return (ret);
}

static t_pixel	*find_start(t_mlx *mlx)
{
	t_pixel	*pix;

	pix = mlx->map->head;
	while (pix && pix->x != mlx->pos->x)
		pix = pix->right;
	while (pix && pix->y != mlx->pos->y)
		pix = pix->down;
	return (pix);
}

int				print_fdf(t_mlx *mlx)
{
	t_pixel *pixel;
	int		cy;

	pixel = find_start(mlx);
	cy = CENTER_Y;
	while (pixel)
	{
		send_pix_right(pixel, cy, mlx);
		send_pix_left(pixel, cy, mlx);
		pixel = pixel->up;
		cy -= mlx->pos->dist;
	}
	pixel = find_start(mlx);
	cy = CENTER_Y;
	while (pixel)
	{
		send_pix_right(pixel, cy, mlx);
		send_pix_left(pixel, cy, mlx);
		pixel = pixel->down;
		cy += mlx->pos->dist;
	}
	return (0);
}
