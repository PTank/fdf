/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:46:02 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:18:34 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static int	fix_pos(t_mlx *mlx)
{
	int	color;

	mlx->pos->y = (mlx->pos->y < 0) ? 0 : mlx->pos->y;
	mlx->pos->x = (mlx->pos->x < 0) ? 0 : mlx->pos->x;
	mlx->pos->y = (mlx->pos->y > mlx->map->h) ? mlx->map->h : mlx->pos->y;
	mlx->pos->x = (mlx->pos->x > mlx->map->w) ? mlx->map->w : mlx->pos->x;
	mlx->pos->dist = (mlx->pos->dist < 1) ? 1 : mlx->pos->dist;
	mlx->pos->dist = (mlx->pos->dist >
			(SIZE_Y_FDF / 2)) ? (SIZE_Y_FDF / 2) : mlx->pos->dist;
	color = mlx_get_color_value(mlx->mlx, ROYALBLUE);
	color_all_img(mlx->fdf->addr, color, mlx->fdf->sizeline, SIZE_Y_FDF);
	print_fdf(mlx);
	put_fdf_img(mlx);
	return (0);
}

static int	key_action(int keyword, t_mlx *mlx)
{
	if (keyword == K_ESC)
		exit_fdf(mlx);
	if (keyword == K_UP)
		mlx->pos->y += 1;
	if (keyword == K_DOWN)
		mlx->pos->y -= 1;
	if (keyword == K_RIGHT)
		mlx->pos->x -= 1;
	if (keyword == K_LEFT)
		mlx->pos->x += 1;
	if (keyword == K_ADD)
		mlx->pos->dist += 1;
	if (keyword == K_SUB)
		mlx->pos->dist -= 1;
	fix_pos(mlx);
	return (0);
}

static int	expose_event(t_mlx *mlx)
{
	put_menu(mlx);
	put_fdf_img(mlx);
	return (0);
}

int			loop(t_mlx *mlx)
{
	init_menu(mlx);
	init_fdf_img(mlx);
	mlx_key_hook(mlx->mlx_win, &key_action, mlx);
	mlx_expose_hook(mlx->mlx_win, &expose_event, mlx);
	mlx_loop(mlx->mlx);
	return (0);
}
