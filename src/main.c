/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:46:16 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:21:32 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

static int		usage(int argc, char **argv)
{
	if (argc != 2)
	{
		ft_putstr("usage: ");
		ft_putstr(argv[0]);
		ft_putstr(" file.fdf\n");
		return (1);
	}
	return (0);
}

static t_mlx	*init_win(void)
{
	t_mlx	*mlx;

	mlx = (t_mlx *)ft_memalloc(sizeof(t_mlx));
	if (mlx)
	{
		mlx->mlx = NULL;
		mlx->mlx_win = NULL;
		mlx->type_of_view = ISO;
		mlx->mlx = mlx_init();
		if (mlx->mlx)
		{
			mlx->mlx_win = mlx_new_window(
					mlx->mlx, SIZE_X, SIZE_Y, TITLE);
			if (mlx->mlx_win)
			{
				return (mlx);
			}
		}
	}
	if (mlx)
		ft_memdel((void **)&mlx);
	perror("Error: ");
	return (NULL);
}

static t_mlx	*init_pos(t_mlx *mlx)
{
	int	disty;
	int	y;
	int	size_x;

	y = SIZE_Y_FDF;
	mlx->pos = (t_pos *)ft_memalloc(sizeof(t_pos));
	if (!mlx->pos)
		return (NULL);
	size_x = SIZE_X_FDF;
	mlx->pos->x = mlx->map->w / 2;
	mlx->pos->y = mlx->map->h / 2;
	mlx->pos->dist = size_x / mlx->map->w;
	disty = y / mlx->map->h;
	if (mlx->pos->dist > disty)
		mlx->pos->dist = disty;
	if (mlx->pos->dist < 10)
		mlx->pos->dist = 10;
	return (mlx);
}

int				main(int argc, char **argv)
{
	t_mlx	*mlx;

	if (usage(argc, argv))
		return (0);
	mlx = init_win();
	if (!mlx)
		return (-1);
	if ((mlx->map = parse_file_to_map(argv[1])))
	{
		init_pos(mlx);
		loop(mlx);
	}
	else
		ft_putendl("Error: file not conform");
	exit_fdf(mlx);
	return (0);
}
