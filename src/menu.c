/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:46:41 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:22:03 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int	init_menu(t_mlx *mlx)
{
	int	color;

	mlx->menu = (t_img *)ft_memalloc(sizeof(t_img));
	if (!mlx->menu)
		return (1);
	mlx->menu->img = mlx_new_image(mlx->mlx, SIZE_X_MENU, SIZE_Y_MENU);
	mlx->menu->addr = mlx_get_data_addr(mlx->menu->img, &mlx->menu->bpp,
			&mlx->menu->sizeline, &mlx->menu->endian);
	color = mlx_get_color_value(mlx->mlx, DIMGREY);
	color_all_img(mlx->menu->addr, color, mlx->menu->sizeline, SIZE_Y_MENU);
	return (0);
}

int	put_menu(t_mlx *mlx)
{
	mlx_put_image_to_window(mlx->mlx, mlx->mlx_win,
			mlx->menu->img, X_MENU, Y_MENU);
	return (0);
}

int	destroy_img(t_img *img)
{
	ft_memdel((void **)&(img->img));
	ft_memdel((void **)&img);
	return (0);
}
