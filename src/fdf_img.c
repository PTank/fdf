/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_img.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:45:33 by akazian           #+#    #+#             */
/*   Updated: 2016/08/25 18:22:22 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

int	init_fdf_img(t_mlx *mlx)
{
	int	color;

	mlx->fdf = (t_img *)ft_memalloc(sizeof(t_img));
	if (!mlx->fdf)
		return (1);
	mlx->fdf->img = mlx_new_image(mlx->mlx, SIZE_X_FDF, SIZE_Y_FDF);
	mlx->fdf->addr = mlx_get_data_addr(mlx->fdf->img, &mlx->fdf->bpp,
			&mlx->fdf->sizeline, &mlx->fdf->endian);
	color = mlx_get_color_value(mlx->mlx, ROYALBLUE);
	color_all_img(mlx->fdf->addr, color, mlx->fdf->sizeline, SIZE_Y_FDF);
	print_fdf(mlx);
	return (0);
}

int	put_fdf_img(t_mlx *mlx)
{
	mlx_put_image_to_window(mlx->mlx, mlx->mlx_win,
			mlx->fdf->img, X_FDF, Y_FDF);
	return (0);
}
