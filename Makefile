# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: akazian <akazian@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 16:00:35 by akazian           #+#    #+#              #
#    Updated: 2016/08/25 17:35:49 by akazian          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			=	fdf
CC				=	gcc
CFLAGS			=	-Wall -Wextra -Werror -g3
LIBFTDIR		=	./libft/
LIBFTH			=	-I$(LIBFTDIR)
MLXDIR			=	./minilibx_macos/
MLX				=	-I$(MLXDIR)
LIBXFLAGS		=	-L$(MLXDIR) -lmlx -framework OpenGL -framework AppKit
LIBFTFLAGS		=	-L$(LIBFTDIR) -lft
INCS_DIR		=	include
OBJS_DIR		=	objects
SRCS_DIR		=	src
SRCS			=	main.c\
				loop.c\
				put_color.c\
				menu.c\
				fdf_img.c\
				draw_line.c\
				map.c\
				pixel.c\
				print_fdf.c\
				iso.c\
				send_pix.c\
				exit.c

OBJS 			=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))

all				:	$(NAME)

$(NAME)			:	$(OBJS_DIR) $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LIBFTFLAGS) $(LIBXFLAGS)

$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INCS_DIR) $(LIBFTH) $(MLX)

$(OBJS_DIR)	:	makelibft
	@mkdir -p $(OBJS_DIR)

makelibft:
	@make -C $(LIBFTDIR)
	@make -C $(MLXDIR)

.PHONY: clean all re fclean

fclean			:	clean
	rm -f $(NAME)

clean			:
	rm -rf $(OBJS_DIR)

re				:	fclean all
