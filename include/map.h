/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:47:53 by akazian           #+#    #+#             */
/*   Updated: 2016/09/01 12:59:11 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_H
# define MAP_H

# include <unistd.h>
# include <struct_map.h>

int		destroy_map(t_map *map);
t_map	*parse_file_to_map(char *filename);

t_pixel	*init_pixel(int y, int x, int z);
t_pixel	*add_node(t_pixel *old, t_pixel *current, t_map *map, int x);

#endif
