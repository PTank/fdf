/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:47:45 by akazian           #+#    #+#             */
/*   Updated: 2016/09/01 13:01:00 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <stdio.h>
# include <stdlib.h>
# include <errno.h>
# include <fcntl.h>
# include <mlx.h>
# include <libft.h>
# include <map.h>

# define SIZE_X 1000
# define SIZE_Y 500
# define TITLE "FDF"

# define SIZE_X_MENU SIZE_X
# define SIZE_Y_MENU (SIZE_Y / 4)
# define X_MENU 0
# define Y_MENU (SIZE_Y - SIZE_Y_MENU)

# define SIZE_X_FDF SIZE_X
# define SIZE_Y_FDF (SIZE_Y - SIZE_Y_MENU)
# define CENTER_X (SIZE_X_FDF / 2)
# define CENTER_Y (SIZE_Y_FDF / 2)
# define X_FDF 0
# define Y_FDF 0
# define K_ESC 53
# define K_UP 126
# define K_DOWN 125
# define K_RIGHT 123
# define K_LEFT 124
# define K_ADD 69
# define K_SUB 78

# include <color.h>

# define ISO 0

# include <struct_fdf.h>

int	loop(t_mlx *mlx);
int	exit_fdf(t_mlx *mlx);

int	put_color(char *addr, int color);
int	color_all_img(char *addr, int color, int sizeline, int y);
int	put_color_xy(t_img *img, int color, int x, int y);

int	init_menu(t_mlx *mlx);
int	put_menu(t_mlx *mlx);
int	init_fdf_img(t_mlx *mlx);
int	put_fdf_img(t_mlx *mlx);
int	destroy_img(t_img *img);

int	print_fdf(t_mlx *mlx);
int	draw_line(t_iso *iso, t_mlx *mlx, t_img *img);
int	iso_simple(t_pixel *pix, t_iso *iso, t_mlx *mlx);
int	make_iso(t_pixel *pix1, t_pixel *pix2, t_mlx *mlx);
int	send_pix_right(t_pixel *pix, int cy, t_mlx *mlx);
int	send_pix_left(t_pixel *pix, int cy, t_mlx *mlx);

#endif
