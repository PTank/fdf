/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct_fdf.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 12:59:52 by akazian           #+#    #+#             */
/*   Updated: 2016/09/01 13:02:21 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_FDF_H
# define STRUCT_FDF_H

typedef struct	s_img
{
	void		*img;
	char		*addr;
	int			bpp;
	int			sizeline;
	int			endian;
}				t_img;

typedef struct	s_pos
{
	int			x;
	int			y;
	int			dist;
}				t_pos;

typedef struct	s_mlx
{
	void		*mlx;
	void		*mlx_win;
	int			*type_of_view;
	t_img		*menu;
	t_img		*fdf;
	t_map		*map;
	t_pos		*pos;
}				t_mlx;

#endif
