/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 12:57:00 by akazian           #+#    #+#             */
/*   Updated: 2016/09/01 13:05:36 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_MAP_H
# define STRUCT_MAP_H

typedef struct		s_pixel
{
	int				z;
	int				x;
	int				y;
	struct s_pixel	*up;
	struct s_pixel	*down;
	struct s_pixel	*left;
	struct s_pixel	*right;
}					t_pixel;

typedef struct		s_map
{
	t_pixel			*head;
	int				h;
	int				w;
}					t_map;

typedef struct		s_iso
{
	int				x1;
	int				y1;
	int				z1;
	int				x2;
	int				y2;
	int				z2;
}					t_iso;

#endif
