/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 17:47:38 by akazian           #+#    #+#             */
/*   Updated: 2016/09/01 13:05:54 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COLOR_H
# define COLOR_H

# define SNOW  0xfffafa
# define GHOSTWHITE  0xf8f8ff
# define WHITESMOKE  0xf5f5f5
# define GAINSBORO  0xdcdcdc
# define FLORALWHITE  0xfffaf0
# define OLDLACE  0xfdf5e6
# define LINEN  0xfaf0e6
# define ANTIQUEWHITE  0xfaebd7
# define PAPAYAWHIP  0xffefd5
# define BLANCHEDALMOND  0xffebcd
# define BISQUE  0xffe4c4
# define PEACHPUFF  0xffdab9
# define NAVAJOWHITE  0xffdead
# define MOCCASIN  0xffe4b5
# define CORNSILK  0xfff8dc
# define IVORY  0xfffff0
# define LEMONCHIFFON  0xfffacd
# define SEASHELL  0xfff5ee
# define HONEYDEW  0xf0fff0
# define MINTCREAM  0xf5fffa
# define AZURE  0xf0ffff
# define ALICEBLUE  0xf0f8ff
# define LAVENDER  0xe6e6fa
# define LAVENDERBLUSH  0xfff0f5
# define MISTYROSE  0xffe4e1
# define WHITE  0xffffff
# define BLACK  0x0
# define DARKSLATEGRAY  0x2f4f4f
# define DIMGREY  0x696969
# define SLATEGREY  0x708090
# define LIGHTSLATEGREY  0x778899
# define GREY  0xbebebe
# define LIGHTGREY  0xd3d3d3
# define MIDNIGHTBLUE  0x191970
# define NAVY  0x80
# define CORNFLOWERBLUE  0x6495ed
# define DARKSLATEBLUE  0x483d8b
# define SLATEBLUE  0x6a5acd
# define MEDIUMSLATEBLUE  0x7b68ee
# define LIGHTSLATEBLUE  0x8470ff
# define MEDIUMBLUE  0xcd
# define ROYALBLUE  0x4169e1
# define BLUE  0xff
# define DODGER BLUE  0x1e90ff
# define DODGERBLUE  0x1e90ff
# define DEEPSKYBLUE  0xbfff
# define SKYBLUE  0x87ceeb
# define LIGHTSKYBLUE  0x87cefa
# define STEELBLUE  0x4682b4
# define LIGHTSTEELBLUE  0xb0c4de
# define LIGHTBLUE  0xadd8e6
# define POWDERBLUE  0xb0e0e6
# define PALETURQUOISE  0xafeeee
# define DARKTURQUOISE  0xced1
# define MEDIUMTURQUOISE  0x48d1cc
# define TURQUOISE  0x40e0d0
# define CYAN  0xffff
# define LIGHTCYAN  0xe0ffff
# define CADETBLUE  0x5f9ea0
# define MEDIUMAQUAMARINE  0x66cdaa
# define AQUAMARINE  0x7fffd4
# define DARKGREEN  0x6400
# define DARKOLIVEGREEN  0x556b2f
# define DARKSEAGREEN  0x8fbc8f
# define SEAGREEN  0x2e8b57
# define MEDIUMSEAGREEN  0x3cb371
# define LIGHTSEAGREEN  0x20b2aa
# define PALEGREEN  0x98fb98
# define SPRINGGREEN  0xff7f
# define LAWNGREEN  0x7cfc00
# define GREEN  0xff00
# define CHARTREUSE  0x7fff00
# define MEDIUMSPRINGGREEN  0xfa9a
# define GREENYELLOW  0xadff2f
# define LIMEGREEN  0x32cd32
# define YELLOWGREEN  0x9acd32
# define FORESTGREEN  0x228b22
# define OLIVEDRAB  0x6b8e23
# define DARKKHAKI  0xbdb76b
# define KHAKI  0xf0e68c
# define PALEGOLDENROD  0xeee8aa
# define LIGHTGOLDENRODYELLOW  0xfafad2
# define LIGHTYELLOW  0xffffe0
# define YELLOW  0xffff00
# define GOLD  0xffd700
# define LIGHTGOLDENROD  0xeedd82
# define GOLDENROD  0xdaa520
# define DARKGOLDENROD  0xb8860b
# define ROSYBROWN  0xbc8f8f
# define INDIANRED  0xcd5c5c
# define SADDLEBROWN  0x8b4513
# define SIENNA  0xa0522d
# define PERU  0xcd853f
# define BURLYWOOD  0xdeb887
# define BEIGE  0xf5f5dc
# define WHEAT  0xf5deb3
# define SANDYBROWN  0xf4a460
# define TAN  0xd2b48c
# define CHOCOLATE  0xd2691e
# define FIREBRICK  0xb22222
# define BROWN  0xa52a2a
# define DARKSALMON  0xe9967a
# define SALMON  0xfa8072
# define LIGHTSALMON  0xffa07a
# define ORANGE  0xffa500
# define DARKORANGE  0xff8c00
# define CORAL  0xff7f50
# define LIGHTCORAL  0xf08080
# define TOMATO  0xff6347
# define ORANGERED  0xff4500
# define RED  0xff0000
# define HOTPINK  0xff69b4
# define DEEPPINK  0xff1493
# define PINK  0xffc0cb

#endif
