/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 16:37:09 by akazian           #+#    #+#             */
/*   Updated: 2015/01/21 13:18:05 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	char	*memory;

	memory = (char *)malloc(size * sizeof(char));
	if (memory == NULL)
		return (NULL);
	ft_bzero(memory, size);
	return (memory);
}
