/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 12:36:22 by akazian           #+#    #+#             */
/*   Updated: 2015/01/21 13:13:28 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*white_space(const char *str)
{
	while (*str == ' ' || *str == '\n' || *str == '\t'
			|| *str == '\v' || *str == '\f' || *str == '\r')
		str++;
	return ((char *)str);
}

int			ft_atoi(const char *str)
{
	int		res;
	int		neg;
	int		i;

	res = 0;
	i = 0;
	neg = 0;
	str = white_space(str);
	while (*str == '-' || *str == '+')
	{
		if (*str == '-')
			neg = 1;
		i++;
		str++;
	}
	while (*str >= '0' && *str <= '9' && i <= 1)
	{
		res *= 10;
		res += *str - '0';
		str++;
	}
	if (neg)
		res *= -1;
	return (res);
}
