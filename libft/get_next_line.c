/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/14 13:34:18 by akazian           #+#    #+#             */
/*   Updated: 2015/01/22 15:47:34 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

static char	*search_n(char *buf, int rd, int *pos)
{
	int		i;
	char	*ret;

	i = *pos;
	while (i < rd)
	{
		if (buf[i] == '\n')
		{
			ret = ft_strsub((char const *)buf, *pos, (i - *pos));
			*pos = i + 1;
			return (ret);
		}
		i++;
	}
	return (buf);
}

static char	*line_cat(char *line, char *src, int rd)
{
	char	*tmp;
	int		i;
	int		j;

	if (line == NULL)
		return (ft_strsub((char const *)src, 0, ft_strlenmax(src, rd)));
	j = 0;
	tmp = line;
	i = ft_strlen(line);
	line = ft_strnew(i + ft_strlenmax(src, rd));
	ft_strcpy(line, tmp);
	free(tmp);
	while (src[j] != '\0' && j < rd)
	{
		line[i] = src[j];
		j++;
		i++;
	}
	line[i] = '\0';
	return (line);
}

static int	rd_value(int const fd, char **line, int *pos, char *buf)
{
	int		rd;
	char	*n_found;

	rd = 0;
	while ((rd = read(fd, buf, BUFF_SIZE)) > 0)
	{
		*pos = 0;
		n_found = search_n(buf, rd, pos);
		if (n_found != buf)
		{
			*line = line_cat(*line, n_found, rd);
			free(n_found);
			return (rd);
		}
		if (n_found == buf)
			*line = line_cat(*line, buf, rd);
	}
	return (rd);
}

static int	free_gnl(t_gnl **gnl)
{
	if (*gnl)
	{
		free(*gnl);
		*gnl = NULL;
	}
	return (1);
}

int			get_next_line(int const fd, char **line)
{
	static	t_gnl	*gnl[MAX_FD] = {NULL};

	if (line == NULL || (fd < 0 || fd > MAX_FD))
		return (-1);
	if (gnl[fd] == NULL && (gnl[fd] = malloc(sizeof(t_gnl))))
	{
		gnl[fd]->rd = 0;
		gnl[fd]->pos = 0;
	}
	if (!(*line = NULL) && gnl[fd]->pos < gnl[fd]->rd)
	{
		if ((*line = search_n(gnl[fd]->buf, gnl[fd]->rd, &gnl[fd]->pos))
				!= gnl[fd]->buf)
			return (1);
		*line = ft_strsub((const char*)gnl[fd]->buf
					, gnl[fd]->pos, gnl[fd]->rd - gnl[fd]->pos);
	}
	gnl[fd]->rd = rd_value(fd, line, &gnl[fd]->pos, gnl[fd]->buf);
	if (gnl[fd]->rd == -1 && free_gnl(&gnl[fd]))
		return (-1);
	if (gnl[fd]->rd == 0 && *line == NULL && free_gnl(&gnl[fd]))
		return (0);
	return (1);
}
