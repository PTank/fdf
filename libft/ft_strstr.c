/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 11:57:14 by akazian           #+#    #+#             */
/*   Updated: 2015/01/21 13:06:40 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strstr(const char *s1, const char *s2)
{
	size_t	i;
	size_t	res;

	i = ft_strlen(s2);
	if (i == 0)
		return ((char *)s1);
	while (*s1 != '\0')
	{
		if (*s1 == *s2)
		{
			res = 0;
			while (*s1 == *s2 && *s2 != '\0')
			{
				s1++;
				s2++;
				res++;
			}
			if (res == i)
				return ((char *)s1 - res);
			s1 = s1 - res;
			s2 = s2 - res;
		}
		s1++;
	}
	return (NULL);
}
