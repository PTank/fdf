/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:56:01 by akazian           #+#    #+#             */
/*   Updated: 2015/01/21 13:18:38 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*src1;
	unsigned char	*src2;
	int				diff;

	src1 = (unsigned char *)s1;
	src2 = (unsigned char *)s2;
	diff = 0;
	if (ft_strlen(s1) == 0 && ft_strlen(s2) == 0)
		return (0);
	while (*src1 == *src2 && n > 0)
	{
		src1++;
		src2++;
		n--;
	}
	if (n != 0)
	{
		diff = *src1 - *src2;
		return (diff);
	}
	return (0);
}
