/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 12:07:22 by akazian           #+#    #+#             */
/*   Updated: 2015/01/21 13:10:29 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char		*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	res;

	if (ft_strlen(s2) == 0)
		return ((char *)s1);
	while (*s1 != '\0' && n)
	{
		if (*s1 == *s2)
		{
			res = 0;
			while (*s1 == *s2 && *s2 != '\0' && n - res != 0)
			{
				s1++;
				s2++;
				res++;
			}
			if (res == ft_strlen(s2 - res))
				return ((char *)s1 - res);
			s1 = s1 - res;
			s2 = s2 - res;
		}
		n--;
		s1++;
	}
	return (NULL);
}
