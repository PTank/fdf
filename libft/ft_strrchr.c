/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 11:51:00 by akazian           #+#    #+#             */
/*   Updated: 2015/01/21 13:07:27 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strrchr(const char *s, int c)
{
	char	*loc;
	size_t	i;
	size_t	k;
	size_t	j;

	k = 0;
	j = 0;
	i = ft_strlen(s) + 1;
	if (i < 2)
		return (NULL);
	while (i)
	{
		if (s[k] == (char)c)
		{
			loc = (char *)s + k;
			j++;
		}
		k++;
		i--;
	}
	if (!j)
		return (NULL);
	return (loc);
}
