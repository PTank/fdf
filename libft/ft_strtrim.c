/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 20:40:32 by akazian           #+#    #+#             */
/*   Updated: 2014/01/12 21:18:13 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	white_space_start(char const *s)
{
	size_t	i;

	i = 0;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && s[i] != '\0')
		i++;
	return (i);
}

char			*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	j;
	char	*cleanstr;

	if (!s)
		return (NULL);
	i = white_space_start(s);
	j = ft_strlen(s);
	if (ft_strlen(s) - i != 0)
	{
		while ((s[j - 1] == ' ' || s[j - 1] == '\n' || s[j - 1] == '\t') && j)
			j--;
	}
	if (ft_strlen(s) == j - i)
	{
		cleanstr = ft_strnew(ft_strlen(s));
		ft_strcpy(cleanstr, s);
		return (cleanstr);
	}
	cleanstr = ft_strnew(j - i);
	ft_strncpy(cleanstr, (s + i), j - i);
	return (cleanstr);
}
